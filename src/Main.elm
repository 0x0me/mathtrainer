module Main exposing (..)

import Html exposing (Html, text, div, h1, p, form, label, br, span)
import Html.Attributes exposing (..)

import Bootstrap.Form.InputGroup as InputGroup
import Bootstrap.Form.Input as Input
import Bootstrap.Grid as Grid
--import Bootstrap.Card as Card
--import Bootstrap.Button as Button

---- MODEL ----


type alias Model =
    {}


init : ( Model, Cmd Msg )
init =
    ( {}, Cmd.none )



---- UPDATE ----


type Msg
    = NoOp


update : Msg -> Model -> ( Model, Cmd Msg )
update msg model =
    ( model, Cmd.none )



---- VIEW ----


view : Model -> Html Msg
view model = 
    Grid.containerFluid [] 
        [ h1 [] [text "Mathetrainer"]
        , p [class "lead"] [text "Hier kannst Du die Grundrechenarten üben."]
        -- render input form for players name
        , Html.form []
            [ div [class "form-group row"]
                [ label [for "player_name", class "col-sm-2 col-form-label"] [text "Name des Spielers"]
                , div [class "col-sm-10"]
                    [ InputGroup.config 
                        (InputGroup.text [ Input.placeholder "Hier kannst Du Deinen Namen eintragen"])
                        |> InputGroup.view
                    ]
                ]
            ]
        , div [class "jumbotron"] 
            [ h1 [] [text "Hallo <Name>, hier ist Dein erste Aufgabe:"] 
            , div []
                [ span [] [text "1"]
                , span [] [text "+"]
                , span [] [text "1"]
                , span [] [text "="]
                , span [] [text "2"]    
                ]
            ]
        ]
    



---- PROGRAM ----


main : Program Never Model Msg
main =
    Html.program
        { view = view
        , init = init
        , update = update
        , subscriptions = always Sub.none
        }
